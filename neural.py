import csv
import os
import sys
import copy
import tensorflow as tf
from sklearn.model_selection import train_test_split

      
def load_data(filename):
    """
    Load shopping data from a CSV file `filename` and convert into a list of
    evidence lists and a list of labels. Return a tuple (evidence, labels).

    evidence should be a list of lists, where each list contains the
    following values, in order:
        - Administrative, an integer
        - Administrative_Duration, a floating point number
        - Informational, an integer
        - Informational_Duration, a floating point number
        - ProductRelated, an integer
        - ProductRelated_Duration, a floating point number
        - BounceRates, a floating point number
        - ExitRates, a floating point number
        - PageValues, a floating point number
        - SpecialDay, a floating point number
        - Month, an index from 0 (January) to 11 (December)
        - OperatingSystems, an integer
        - Browser, an integer
        - Region, an integer
        - TrafficType, an integer
        - VisitorType, an integer 0 (not returning) or 1 (returning)
        - Weekend, an integer 0 (if false) or 1 (if true)

    labels should be the corresponding list of labels, where each label
    is 1 if Revenue is true, and 0 otherwise.
    """
    index={
        "int":{0,2,4,11,12,13,14},
        "float":{1,3,5,6,7,8,9},
        "month":{"Jan":0,
                 "Feb":1,
                 "Mar":2,
                 "Apr":3,
                 "May":4,
                 "June":5,
                 "Jul":6,
                 "Aug":7,
                 "Sep":8,
                 "Oct":9,
                 "Nov":10,
                 "Dec":11
                 },
        "FALSE":0,
        "TRUE":1
        }
            
    evidence=[]
    labels=[]
    k=0
    with open(filename)as f:
        contents=csv.reader(f)
        next(contents)
        for row in contents:
            tevidence=[]
            for i in range(len(row)):
                if i in index['int']:
                    tevidence.insert(i,int(row[i]))
                elif i in index['float']:
                    tevidence.insert(i,float(row[i]))
                elif i==10:
                    tevidence.insert(i,index["month"][row[i]])
                elif i==15:
                    if row[i]=='Returning_Visitor':
                        tevidence.insert(i,1)
                    else:
                        tevidence.insert(i,0)
                elif i==16:
                    tevidence.insert(i,index[row[i]])
                elif i==17:
                    labels.append(index[row[i]])
            evidence.append(copy.deepcopy(tevidence))
    
    return (evidence,labels)       
    #raise NotImplementedError

parameters, labels=load_data(sys.argv[1])

X_train,X_test,Y_train,Y_test=train_test_split(parameters, labels,test_size=0.2)

model=tf.keras.Sequential([
    tf.keras.layers.Dense(17, activation='relu', input_shape=(17,)),
    tf.keras.layers.Dense(51, activation='relu'),
    tf.keras.layers.Dense(34, activation='relu'),
    tf.keras.layers.Dropout(0.2),
    tf.keras.layers.Dense(1, activation='sigmoid')
    ])

model.compile(
        optimizer="adam",
        loss='binary_crossentropy', 
        metrics=["accuracy"]
    )

model.fit(X_train,Y_train,epochs=10)
model.evaluate(X_test,  Y_test, verbose=2)

