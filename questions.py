import nltk
import sys
import string
import os
from nltk.tokenize import word_tokenize
import math

FILE_MATCHES = 1
SENTENCE_MATCHES = 1

#nltk.download ('punkt')
#nltk.download('stopwords')
def main():

    # Check command-line arguments
    if len(sys.argv) != 2:
        sys.exit("Usage: python questions.py corpus")

    # Calculate IDF values across files
    files = load_files(sys.argv[1])
    file_words = {
        filename: tokenize(files[filename])
        for filename in files
    }
    file_idfs = compute_idfs(file_words)

    # Prompt user for query
    query = set(tokenize(input("Query: ")))

    # Determine top file matches according to TF-IDF
    filenames = top_files(query, file_words, file_idfs, n=FILE_MATCHES)

    # Extract sentences from top files
    sentences = dict()
    for filename in filenames:
        for passage in files[filename].split("\n"):
            for sentence in nltk.sent_tokenize(passage):
                tokens = tokenize(sentence)
                if tokens:
                    sentences[sentence] = tokens

    # Compute IDF values across sentences
    idfs = compute_idfs(sentences)

    # Determine top sentence matches
    matches = top_sentences(query, sentences, idfs, n=SENTENCE_MATCHES)
    for match in matches:
        print(match)


def load_files(directory):
    """
    Given a directory name, return a dictionary mapping the filename of each
    `.txt` file inside that directory to the file's contents as a string.
    """
    fil={}
    for i in os.listdir(directory):
                path=os.path.join(directory,i)
                data=open(path,'r',errors='ignore')
                fil[i]=data.read()
                data.close()
    return fil
    #raise NotImplementedError


def tokenize(document):
    """
    Given a document (represented as a string), return a list of all of the
    words in that document, in order.

    Process document by coverting all words to lowercase, and removing any
    punctuation or English stopwords.
    """
    lis=(word_tokenize(document.lower()))
    new=[]
    for i in lis:
        if i in string.punctuation or i in nltk.corpus.stopwords.words("english"):
            continue
        new.append(i)
    return new
    #raise NotImplementedError


def compute_idfs(documents):
    """
    Given a dictionary of `documents` that maps names of documents to a list
    of words, return a dictionary that maps words to their IDF values.

    Any word that appears in at least one of the documents should be in the
    resulting dictionary.
    """
    idf={}
    lis=[]
    for j in documents:
        lis+=documents[j]
    lis=set(lis)
    for i in lis:
            num=0
            for k in documents:
                    if i in documents[k]:
                        num+=1
            idf[i]=math.log10(len(documents)/num)
        
    return idf
            
    #raise NotImplementedError


def top_files(query, files, idfs, n):
    """
    Given a `query` (a set of words), `files` (a dictionary mapping names of
    files to a list of their words), and `idfs` (a dictionary mapping words
    to their IDF values), return a list of the filenames of the the `n` top
    files that match the query, ranked according to tf-idf.
    """
    lis=[]
    for i in files:
        num=0
        for j in query:
            num+=(files[i].count(j)*idfs[j])
        lis.append((num,i))
    lis.sort(reverse=True)
    return ([i[1] for i in lis[0:n]])

    #raise NotImplementedError


def top_sentences(query, sentences, idfs, n):
    """
    Given a `query` (a set of words), `sentences` (a dictionary mapping
    sentences to a list of their words), and `idfs` (a dictionary mapping words
    to their IDF values), return a list of the `n` top sentences that match
    the query, ranked according to idf. If there are ties, preference should
    be given to sentences that have a higher query term density.
    """
    lis=[]
    for i in sentences:
        idf=0
        number=0
        for j in query:
            if j in sentences[i]:
                    idf+=idfs[j]
                    number+=1
        lis.append((idf,number/len(sentences[i]),i))
    lis.sort(reverse=True)
    return ([i[2] for i in lis[0:n]])

    #raise NotImplementedError


if __name__ == "__main__":
    main()
