"""
Tic Tac Toe Player
"""

import math
import random
import copy

X = "X"
O = "O"
EMPTY = None
#next=None

def initial_state():
    """
    Returns starting state of the board.
    """
    return [[EMPTY, EMPTY, EMPTY],
            [EMPTY, EMPTY, EMPTY],
            [EMPTY, EMPTY, EMPTY]]


def player(board):
    """
    Returns player who has the next turn on a board.
    """
    '''global next
    if board==initial_state():
        next=X
        return next
    if next==X:
        next=O
    elif next==O:
        next=X
'''
    sum=0
    for i in range(len(board)):
        sum=sum + board[i].count(EMPTY)
    if sum%2!=0:
        return X
    else:
        return O
    #raise NotImplementedError


def actions(board):
    """
    Returns set of all possible actions (i, j) available on the board.
    """
    set=()
    for i in range(len(board)):
      for j in range(len(board[i])):
        if board[i][j]!=X and board[i][j]!=O:
            set=set + ((i, j),)
    return set
    #raise NotImplementedError


def result(board, action):
    """
    Returns the board that results from making move (i, j) on the board.
    """
    for i,j in actions(board):
            if action==(i,j):
                parent=copy.deepcopy(board)
                parent[i][j]=player(board)
                return parent
    raise Exception
    
    #raise NotImplementedError


def winner(board):
    """
    Returns the winner of the game, if there is one.
    """
    for i in range(len(board)):
        temp=board[i][0]
        if board[i][1]==temp and board[i][2]==temp:
            return temp
    for j in range(3) :
        temp=board[0][j]
        if board[1][j]==temp and board[2][j]==temp:
            return temp
    if board[0][0]==board[1][1] and board[1][1]==board[2][2]:
        return board[2][2]
    elif board[2][0]==board[1][1] and board[1][1]==board[0][2]:
        return board[0][2]
                       
    return None
    #raise NotImplementedError


def terminal(board):
    """
    Returns True if game is over, False otherwise.
    """
    if winner(board)==X or winner(board)==O or actions(board)==():
        return True
    else:
        return False
    #raise NotImplementedError


def utility(board):
    """
    Returns 1 if X has won the game, -1 if O has won, 0 otherwise.
    """
    if terminal(board) is True:
        if winner(board)==X:
            return 1
        elif winner(board)==O:
            return -1
        elif actions(board)==():
            return 0
    else:
        return None
    #raise NotImplementedError



def minimax(board):
    """
    Returns the optimal action for the current player on the board.
    """

    if terminal(board):
        return None
    Max =-99
    Min = 99

    if player(board) == X:
        return MaxVal(board, Max, Min)[1]
    else:
        return MinVal(board, Max, Min)[1]

def MaxVal(board, Max, Min):
    move = None
    if terminal(board):
        return [utility(board), None];
    v = -99
    for action in actions(board):
        test = MinVal(result(board, action), Max, Min)[0]
        Max = max(Max, test)
        if test > v:
            v = test
            move = action
        if Max >= Min:
            break
    return [v, move]

def MinVal(board, Max, Min):
    move = None
    if terminal(board):
        return [utility(board), None];
    v = 99
    for action in actions(board):
        test = MaxVal(result(board, action), Max, Min)[0]
        Min = min(Min, test)
        if test < v:
            v = test
            move = action
        if Max >= Min:
            break
    return [v, move]

